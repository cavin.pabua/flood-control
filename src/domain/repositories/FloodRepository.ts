
export interface FloodRepository {
    GetRecords(callback:any): Promise<any>
    GetRealtimeSwitch(callback:any): Promise<any>
    GetRealtimeRecord(callback:any): Promise<any>
    SwitchDevice(value: boolean, callback:any): Promise<any>
}
