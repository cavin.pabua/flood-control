import { Todo } from "../entities/Todos"
import { TodoRepository } from "../repositories/TodoRepository"

export interface TodoService {
    GetItems(): Promise<Todo[]>
}

export class TodoServiceImpl implements TodoService {
    todoRepo: TodoRepository

    constructor(ir: TodoRepository) {
        this.todoRepo = ir
    }

    async GetItems(): Promise<Todo[]> {
        return this.todoRepo.GetItems()
    }

    async createTodo(data: any) {
        return this.todoRepo.createTodo(data)
    }

    DeleteTodo(data: Todo) {
        if (data.todoIsComplete === true) {
            throw alert("Cannot delete completed todo.")
        } else {
            return this.todoRepo.deleteTodo(data)
        }
    }

    UpdateTodo(data: Todo) {
        const todoUpdateInput = data.todo.trim()
        if (todoUpdateInput.length === 0) {
            throw alert("Update todo should not be empty.")
        } else {
            return this.todoRepo.updateTodo(data)
        }
    }

    CompleteTodo(data: Todo) {
        return this.todoRepo.completeTodo(data)
    }
}
