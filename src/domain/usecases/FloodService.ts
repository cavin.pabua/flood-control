import { FloodRepository } from "../repositories/FloodRepository";

export class FloodService {
    itemRepo: FloodRepository

    constructor(ir: FloodRepository) {
        this.itemRepo = ir
    }

    async GetRecords(callback: any): Promise<any> {
        return this.itemRepo.GetRecords(callback)
    }
    async GetRealtime(callback: any): Promise<any> {
        return this.itemRepo.GetRealtimeRecord(callback)
    }
    async GetSwitch(callback: any): Promise<any> {
        return this.itemRepo.GetRealtimeSwitch(callback)
    }
    async SwitchDevice(value: boolean,callback: any): Promise<any> {
        return this.itemRepo.SwitchDevice(value, callback)
    }
}
