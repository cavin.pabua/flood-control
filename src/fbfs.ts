import firebase from "firebase"

const fbapp = firebase.initializeApp({
    apiKey: "AIzaSyCy4beIaPZR47oPORXvenozmNKXGjzMf08",
    authDomain: "flood-control-af16f.firebaseapp.com",
    projectId: "flood-control-af16f",
    storageBucket: "flood-control-af16f.appspot.com",
    messagingSenderId: "823881122657",
    appId: "1:823881122657:web:fcff6eab312867f546bb20",
    measurementId: "G-L3BGZKS1TY"
})

const db = fbapp.firestore()
export default db
