import db from "../../fbfs"
export class FloodRepositoryImpl {

    async GetRecords(callback: any): Promise<any> {
        await db
            .collection("records").orderBy("timestamp", "desc")
            .onSnapshot(
                async (rec) => {
                    const promises = rec.docs.map((recSnap) => {
                        return { id: recSnap.id, ...recSnap.data() };
                    });
                    const results = await Promise.all(promises);
                    callback && callback(results);
                }
            )
    }

    async GetRealtimeRecord(callback: any): Promise<any> {
        const dataRef = db.collection("realtime").doc("stats")
        dataRef.onSnapshot(async (snap) => {
            const data = snap.data();
            callback && callback({ ...data });
        });
    }

    async GetRealtimeSwitch(callback: any): Promise<any> {
        const dataRef = db.collection("switch").doc("setting")
        dataRef.onSnapshot(async (snap) => {
            const data = snap.data();
            callback && callback({ ...data });
        });
    }

    async SwitchDevice(value: boolean, callback: any): Promise<any> {
        const dataRef = db.collection("switch").doc("setting")
            .set({isOn: value}, {merge: true}).then(()=>{
                callback(value)
                return true
            })
    }
}