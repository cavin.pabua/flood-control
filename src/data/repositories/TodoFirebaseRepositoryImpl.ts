import { Todo } from "../../domain/entities/Todos"
import { TodoRepository } from "../../domain/repositories/TodoRepository"
import db from "../../fbfs"

class TodoDTO {
    todoUID = 0
    todo = ""
    todoIsComplete = false
}

// const todoListLocalStorageName = "todoList"
const todoListFirebaseCollectionName = "todoList"
// const getjsonString = localStorage.getItem(todoListLocalStorageName)
// const parsedJsonString = JSON.parse(getjsonString || "[]")

export class TodoFirebaseRepositoryImpl implements TodoRepository {
    async GetItems(): Promise<Todo[]> {
        const listOftodos: any[] = []

        await db
            .collection(todoListFirebaseCollectionName)
            .get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    listOftodos.push({
                        todoUID: doc.data().todoUID,
                        todo: doc.data().todo,
                        todoIsComplete: doc.data().todoIsComplete,
                    })
                })
            })
        return listOftodos.map(
            (todo: TodoDTO) => new Todo(todo.todoUID, todo.todo, todo.todoIsComplete),
            console.log("lists asa ka--->>>", listOftodos),
        )
    }

    async createTodo(todo: Todo) {
        const numToString = todo.todoUID.toString()
        await db
            .collection(todoListFirebaseCollectionName)
            .doc(numToString)
            .set({
                todoUID: Number(todo.todoUID),
                todo: todo.todo,
                todoIsComplete: todo.todoIsComplete,
            })
        console.log("todo--->>>", todo)
    }

    async deleteTodo(data: Todo) {
        const numToString = data.todoUID.toString()
        await db.collection(todoListFirebaseCollectionName).doc(numToString).delete()
    }

    async updateTodo(data: Todo) {
        const numToString = data.todoUID.toString()
        await db.collection(todoListFirebaseCollectionName).doc(numToString).update({ todo: data.todo })
    }

    async completeTodo(data: Todo) {
        const numToString = data.todoUID.toString()
        await db
            .collection(todoListFirebaseCollectionName)
            .doc(numToString)
            .update({ todoIsComplete: data.todoIsComplete })
    }
}
