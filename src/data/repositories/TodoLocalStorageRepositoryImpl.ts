import { Todo } from "../../domain/entities/Todos"
import { TodoRepository } from "../../domain/repositories/TodoRepository"

class TodoDTO {
    todoUID = 0
    todo = ""
    todoIsComplete = false
}

const todoListLocalStorageName = "todoList"
const getjsonString = localStorage.getItem(todoListLocalStorageName)
const parsedJsonString = JSON.parse(getjsonString || "[]")

export class TodoLocalStorageRepositoryImpl implements TodoRepository {
    async GetItems(): Promise<Todo[]> {
        return parsedJsonString.map((todo: TodoDTO) => new Todo(todo.todoUID, todo.todo, todo.todoIsComplete))
    }

    createTodo(todo: Todo): Todo[] {
        parsedJsonString.push(todo)
        localStorage.setItem(todoListLocalStorageName, JSON.stringify(parsedJsonString))
        console.log("CHECK ME OUT!--->>>", localStorage)
        return parsedJsonString.map((todo: TodoDTO) => new Todo(todo.todoUID, todo.todo, todo.todoIsComplete))
    }

    deleteTodo(data: Todo): Todo[] {
        const parseIntData = parseInt(data.todoUID.toString())
        const indextoDelete = parsedJsonString.findIndex((data: Todo) => data.todoUID === parseIntData)
        parsedJsonString.splice(indextoDelete, 1)
        localStorage.setItem(todoListLocalStorageName, JSON.stringify(parsedJsonString))
        return parsedJsonString.map((todo: TodoDTO) => new Todo(todo.todoUID, todo.todo, todo.todoIsComplete))
    }

    updateTodo(data: Todo): Todo[] {
        const parseIntData = parseInt(data.todoUID.toString())
        const indextoUpdate = parsedJsonString.findIndex((data: Todo) => data.todoUID === parseIntData)
        parsedJsonString[indextoUpdate].todo = data.todo
        localStorage.setItem(todoListLocalStorageName, JSON.stringify(parsedJsonString))
        return parsedJsonString.map((todo: TodoDTO) => new Todo(todo.todoUID, todo.todo, todo.todoIsComplete))
    }

    completeTodo(data: Todo): Todo[] {
        const parseIntData = parseInt(data.todoUID.toString())
        const indextoUpdateTodoStatus = parsedJsonString.findIndex((data: Todo) => data.todoUID === parseIntData)
        parsedJsonString[indextoUpdateTodoStatus].todoIsComplete = data.todoIsComplete
        localStorage.setItem(todoListLocalStorageName, JSON.stringify(parsedJsonString))
        return parsedJsonString.map((todo: TodoDTO) => new Todo(todo.todoUID, todo.todo, todo.todoIsComplete))
    }
}
