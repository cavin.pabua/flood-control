import React from "react"
import { Redirect, BrowserRouter as Router, Switch, Route } from "react-router-dom"
import FloodControl from "./pages/flood-control/FloodControl";

const RouteManager = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={FloodControl} />
                <Redirect from="*" to="/" />
            </Switch>
        </Router>
    )
}

export default RouteManager
