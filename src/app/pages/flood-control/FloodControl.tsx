import React from "react"
import { connect } from "react-redux"
import moment from "moment"
import {getHistoryRecords, getRealtimeRecording ,getRealtimeSwitch, switchDevice} from "../../redux/flood/Item.actions";
import {
    EyeOutlined,
    SlidersOutlined,
    SettingOutlined
} from '@ant-design/icons';
import { Layout, Row, Col, Typography, Table, Divider, Switch, Space,   } from 'antd';
const { Title, Text } = Typography;

const { Header, Footer, Content } = Layout;
interface RootState {
    items: any
}
const FloodControl = () => {
    const [switchSetting, setSwitch] = React.useState(false)
    const [realtimeRecord, setRealtimeRecord] = React.useState(
        {
            distance1:"",
            distance2:"",
            pump: "",
            timestamp: 0,
            valve1: "",
            valve2: ""
        }
        )
    const [historyRecords, setHistoryRecords] = React.useState([])


    function onChange(checked: boolean) {
        switchDevice(checked, (callback: any)=>{
            setSwitch(callback)
        })

    }

    const initializeCollections = () => {
        getHistoryRecords((callback: any)=>{
            setHistoryRecords(callback);
        })
        getRealtimeSwitch((callback: { isOn: boolean; })=>{
            setSwitch(callback.isOn)
        })
        getRealtimeRecording((callback: any)=>{
            setRealtimeRecord(callback)
        })
    }

    React.useEffect(() => {
        initializeCollections()
    }, []);



    const columns = [
        {
            title: 'Sensor 1',
            dataIndex: 'distance1',
            key: 'distance1',
            render: (text: React.ReactNode) => <b>{text} cm</b>,
        },
        {
            title: 'Sensor 2',
            dataIndex: 'distance2',
            key: 'distance2',
            render: (text: React.ReactNode) => <b>{text} cm</b>,
        },
        {
            title: 'Timestamp',
            dataIndex: 'timestamp',
            key: 'timestamp',
            render: (text: number) => <b>{moment.unix(text).format("MMMM D YYYY, h:mm:ss A")}</b>,
        },
    ];

    return (
        <Layout>
            <Header />
            <Content>
                <Layout>
                    <Row>
                        <Col xs={{span:20, offset:2}} sm={{span: 20, offset: 2}} md={{span:12, offset: 6}}  style={{marginTop: "50px", marginBottom: "50px"}}>
                            <div>

                                {switchSetting ? (
                                    <div>
                                        <Title level={5}>Device is turned on</Title>
                                    </div>
                                ) : (
                                    <div>
                                        <Title level={5}>Device is turned off</Title>
                                    </div>
                                )}
                                <Space direction="horizontal">
                                    <Text>Device Switch: </Text>
                                    <Switch checked={switchSetting} onChange={onChange} />
                                </Space>

                            </div>
                            <Divider />
                            <div>
                                <Title level={5}>Real-time view</Title>
                                <Text ><EyeOutlined /> Sensor 1: {realtimeRecord.distance1} cm</Text> <br/>
                                <Text><EyeOutlined /> Sensor 2: {realtimeRecord.distance2} cm</Text> <br/>
                                <Text><SlidersOutlined /> Pump: {realtimeRecord.pump}</Text> <br/>
                                <Text><SettingOutlined /> Valve 1: {realtimeRecord.valve1}</Text> <br/>
                                <Text><SettingOutlined /> Valve 2: {realtimeRecord.valve2}</Text> <br/>
                            </div>

                            <Divider/>
                            <Title level={4}>Historical Records</Title>
                            <Table dataSource={historyRecords} columns={columns} bordered={true} scroll={{ x: 300 }} />

                        </Col>
                    </Row>
                </Layout>
            </Content>

        </Layout>
    )
}

const mapStateToProps = (state: RootState) => {
    console.log(state.items)
    return {
        items: state.items.items,
    }
}

export default connect(mapStateToProps)(FloodControl)
