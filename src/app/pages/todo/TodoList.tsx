import React, { useEffect, useState } from "react"
import { connect, useDispatch } from "react-redux"
// import { refreshList, createTodo } from "../../redux/Todo/Todo.actions"
import { refreshList, createTodo, deleteTodo, updateTodo, completeTodo } from "../../redux/Todo/Todo.actions"
import { TodoProps, Todo } from "../../redux/Todo/Todo.types"
import "./TodoList.css"
import deleteTodoImg from "../../assets/trash logo copy.png"
import updateTodoImg from "../../assets/editlogo copy.png"
import cancelupdateTodoImg from "../../assets/cancel logo.png"
import saveupdateTodoImg from "../../assets/save logo copy.png"

interface RootState {
    todos: any
}
const TodoList = ({ todos }: TodoProps) => {
    const dispatch = useDispatch()

    const [fortodo, setForTodo] = useState("")
    const [forupdateTodo, setForupdateTodo] = useState("")
    const [updatetodoUID, setUpdatetodoUID] = useState(0)
    const [displayInputforUpdate, setDisplayInputforUpdate] = useState(false)
    const [forOrigTodotoUpdate, setForOrigTodotoUpdate] = useState("")
    useEffect(() => {
        dispatch(refreshList)
    }, [dispatch])

    const createNewTodo = () => {
        const dNowforUID = Date.now().toString()
        console.log("createNewTodo-->>", dNowforUID)
        console.log("createNewTodo-->>", fortodo)
        const todoDatas = {
            todoUID: dNowforUID,
            todo: fortodo,
            todoIsComplete: false,
        }
        dispatch(createTodo(todoDatas))
    }

    return (
        <div>
            <form
                autoComplete="off"
                onSubmit={(e) => {
                    e.preventDefault()
                    createNewTodo()
                }}
            >
                <div className="todoListWrapper">
                    <div className="todoContainer">
                        <div className="todoHeader">
                            <h1>TODO LIST</h1>
                        </div>
                        <div className="todoInputContainer">
                            <label>TODO: </label>
                            <input
                                type="text"
                                placeholder="New TODO"
                                value={fortodo}
                                onChange={(e) => setForTodo(e.target.value)}
                            />
                        </div>
                        <div className="todoSubmitContainer">
                            <button type="submit" onClick={createNewTodo}>
                                Add TODO
                            </button>
                        </div>
                        <div className="todoListContainer">
                            {displayInputforUpdate ? (
                                <div className="editTodo">
                                    <input
                                        type="text"
                                        placeholder={forOrigTodotoUpdate}
                                        value={forupdateTodo}
                                        onChange={(e) => setForupdateTodo(e.target.value)}
                                    />
                                    <img
                                        className="saveUpdateTodo"
                                        src={saveupdateTodoImg}
                                        alt="Save Update"
                                        width="100%"
                                        onClick={() => {
                                            const todoDatas = {
                                                todoUID: updatetodoUID,
                                                todo: forupdateTodo,
                                            }
                                            dispatch(updateTodo(todoDatas))
                                            setDisplayInputforUpdate(false)
                                            setForupdateTodo("")
                                        }}
                                    />
                                    <img
                                        className="cancelUpdateTodo"
                                        src={cancelupdateTodoImg}
                                        alt="Cancel Update"
                                        width="100%"
                                        onClick={() => {
                                            setDisplayInputforUpdate(false)
                                            setForupdateTodo("")
                                        }}
                                    />
                                </div>
                            ) : (
                                console.log("Update todo status", displayInputforUpdate)
                            )}
                            <table>
                                <thead>
                                    <tr>
                                        <th>List</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {todos.map((todo: Todo, index) => (
                                        <tr key={index}>
                                            <td
                                                className={todo.todoIsComplete ? "todoStats" : undefined}
                                                onClick={() => {
                                                    const todoStatus = todo.todoIsComplete ? false : true
                                                    const todoDatas = {
                                                        todoUID: todo.todoUID,
                                                        todoIsComplete: todoStatus,
                                                    }
                                                    dispatch(completeTodo(todoDatas))
                                                }}
                                            >
                                                <label>{todo.todo}</label>
                                            </td>
                                            <td className="actionsContainer">
                                                <img
                                                    className="update"
                                                    src={updateTodoImg}
                                                    alt="Update Todo"
                                                    width="100%"
                                                    onClick={() => {
                                                        setDisplayInputforUpdate(true)
                                                            setForOrigTodotoUpdate(todo.todo)
                                                            setUpdatetodoUID(todo.todoUID)
                                                    }}
                                                />
                                                <img
                                                    className="delete"
                                                    src={deleteTodoImg}
                                                    alt="Delete Todo"
                                                    width="100%"
                                                    onClick={() => {
                                                        const todoDatas = {
                                                            todoUID: todo.todoUID,
                                                            todoIsComplete: todo.todoIsComplete,
                                                        }
                                                        dispatch(deleteTodo(todoDatas))
                                                    }}
                                                />
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = (state: RootState) => {
    console.log(state.todos)
    return {
        todos: state.todos.todos,
    }
}

export default connect(mapStateToProps)(TodoList)
