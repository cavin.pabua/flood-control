import {FloodRepositoryImpl} from "../../../data/repositories/FloodRepositoryImpl";
import { FloodService } from "../../../domain/usecases/FloodService";

// Realtime
export const getHistoryRecords = async (callback: any) => {
    const floodRepo = new FloodRepositoryImpl();
    const floodService = new FloodService(floodRepo);
    return await floodService.GetRecords(callback);
};

// Realtime
export const getRealtimeRecording = async (callback: any) => {
    const floodRepo = new FloodRepositoryImpl();
    const floodService = new FloodService(floodRepo);
    return await floodService.GetRealtime(callback);
};

// Realtime
export const getRealtimeSwitch = async (callback: any) => {
    const floodRepo = new FloodRepositoryImpl();
    const floodService = new FloodService(floodRepo);
    return await floodService.GetSwitch(callback);
};

// Realtime
export const switchDevice = async (value: boolean,callback: any) => {
    const floodRepo = new FloodRepositoryImpl();
    const floodService = new FloodService(floodRepo);
    return await floodService.SwitchDevice(value, callback);
};

