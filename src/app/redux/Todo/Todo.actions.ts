import { LIST_LOAD_REQUEST, LIST_LOAD_SUCCESS, LIST_LOAD_FAILURE } from "./Todo.types"
import { TodoServiceImpl } from "../../../domain/usecases/TodosService"
import { TodoFirebaseRepositoryImpl } from "../../../data/repositories/TodoFirebaseRepositoryImpl"
// import { TodoLocalStorageRepositoryImpl } from "../../../data/repositories/TodoLocalStorageRepositoryImpl"

export const refreshList = async (dispatch: any) => {
    dispatch({ type: LIST_LOAD_REQUEST })

    try {
        const todoRepo = new TodoFirebaseRepositoryImpl()
        const todoService = new TodoServiceImpl(todoRepo)
        const todos = await todoService.GetItems()
        dispatch({ type: LIST_LOAD_SUCCESS, payload: todos })
    } catch (error) {
        dispatch({ type: LIST_LOAD_FAILURE, error })
    }
}

export const createTodo = (todo: any) => {
    return async function (dispatch: any) {
        const todoRepo = new TodoFirebaseRepositoryImpl()
        const todoService = new TodoServiceImpl(todoRepo)
        await todoService.createTodo(todo).then(() => {
            dispatch(refreshList)
        })
    }
}

export const deleteTodo = (todo: any) => {
    return async function (dispatch: any) {
        const todoRepo = new TodoFirebaseRepositoryImpl()
        const todoService = new TodoServiceImpl(todoRepo)
        await todoService.DeleteTodo(todo).then(() => {
            dispatch(refreshList)
        })
    }
}

export const updateTodo = (todo: any) => {
    return async function (dispatch: any) {
        const todoRepo = new TodoFirebaseRepositoryImpl()
        const todoService = new TodoServiceImpl(todoRepo)
        await todoService.UpdateTodo(todo).then(() => {
            dispatch(refreshList)
        })
    }
}

export const completeTodo = (todo: any) => {
    return async function (dispatch: any) {
        const todoRepo = new TodoFirebaseRepositoryImpl()
        const todoService = new TodoServiceImpl(todoRepo)
        await todoService.CompleteTodo(todo).then(() => {
            dispatch(refreshList)
        })
    }
}
