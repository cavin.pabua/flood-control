import React from "react"
import { Provider } from "react-redux"
import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from "redux-thunk"

import items from "./app/redux/Item/Item.reducers"
import RouteManager from "./app/RouteManager"
// import users from "./app/redux/User/User.reducers"
// import UserRouteManager from "./app/UserRouteManager"
// import todos from "./app/redux/Todo/Todo.reducers"
import TodoRouteManager from "./app/TodoRouteManager"

// Setup Redux store with Thunks
const reducers = combineReducers({ items })
// const reducers = combineReducers({ users })
// const reducers = combineReducers({ todos })
const store = createStore(reducers, applyMiddleware(thunk))

const App = () => {
    return (
        <Provider store={store}>
             <RouteManager />
            {/* <UserRouteManager /> */}
            {/*<TodoRouteManager />*/}
        </Provider>
    )
}

export default App
